#pragma once
#include <algorithm>
#include <stdexcept>
#include <type_traits>
#include <iostream>

class Array {
public:
  Array() : capacity_{100}, data_{new int[100]}, size_{0} {}

  Array(const std::size_t capacity)
      : capacity_{capacity}, data_{new int[capacity_]}, size_{0} {}

  // Array(const Array &other);
  // Array(Array &&other);
  // Array &operator=(const Array &other);
  // Array &operator=(Array &&other);

  ~Array() { delete[] data_; }

  void push_back(const int &element) {
    if (size_ == capacity_) {
      throw std::out_of_range{"Nemamo vise prostora!"};
    }
    data_[size_++] = element;
  }

  int &operator[](size_t index) {
    return data_[index];
  }

  const int &operator[](size_t index) const {
    return data_[index];
  }

  int &at(size_t index) {
    if (index >= size_) {
      throw std::out_of_range{"Nevalidan indeks!"};
    }
    return data_[index];
  }

  const int &at(size_t index) const {
    if (index >= size_) {
      throw std::out_of_range{"Nevalidan indeks!"};
    }
    return data_[index];
  }

  bool empty() const {
    return size_ == 0;
  }
  size_t size() const {
    return size_;
  }

private:
  std::size_t capacity_;
  int *data_;
  std::size_t size_;
};
